package domain;

public interface IdFinder<T> {
    Integer getId();
}
