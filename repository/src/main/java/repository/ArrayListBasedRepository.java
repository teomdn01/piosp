package repository;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private final List<T> elements;

    public ArrayListBasedRepository(){
        this.elements = new ArrayList<>();
    }
    @Override
    public void add(T element) {
        this.elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.elements.contains(element);
    }

    @Override
    public void remove(T element) {
        this.elements.remove(element);
    }
}
