package repository;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;


public class ECFastListBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> elements;

    public ECFastListBasedRepository() {
        this.elements = new FastList<>();
    }

    @Override
    public void add(T element) {
        this.elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.elements.contains(element);
    }

    @Override
    public void remove(T element) {
        this.elements.remove(element);
    }
}
