package repository;

import com.koloboke.collect.set.hash.HashObjSet;

import static com.koloboke.collect.set.hash.HashObjSets.newMutableSet;

public class KolobokeHashSetBasedRepository<T> implements InMemoryRepository<T> {
    private final HashObjSet<T> elements;

    public KolobokeHashSetBasedRepository() {
        elements = newMutableSet();
    }

    @Override
    public void add(T element) {
        this.elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.elements.contains(element);
    }

    @Override
    public void remove(T element) {
        this.elements.remove(element);
    }
}
