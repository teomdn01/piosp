package repository;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class FastUtilObjectArrayBasedRepository<T> implements InMemoryRepository<T> {
    private final ObjectArrayList<T> elements;

    public FastUtilObjectArrayBasedRepository() {
        elements = new ObjectArrayList<>();
    }

    @Override
    public void add(T element) {
        this.elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.elements.contains(element);
    }

    @Override
    public void remove(T element) {
        this.elements.remove(element);
    }
}
