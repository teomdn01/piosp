package repository;

import domain.IdFinder;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<ID, T extends IdFinder<T>> implements InMemoryRepository<T> {
    ConcurrentHashMap<Integer, T> elements;

    public ConcurrentHashMapBasedRepository() {
        this.elements = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T element) {
        elements.put(element.getId(), element);
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public void remove(T element) {
        elements.remove(element.getId());
    }
}
