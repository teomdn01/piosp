package repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T>{
    private final Set<T> elements;

    public HashSetBasedRepository() {
        this.elements = new HashSet<>();
    }

    @Override
    public void add(T element) {
        this.elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.elements.contains(element);
    }

    @Override
    public void remove(T element) {
        this.elements.remove(element);
    }
}
