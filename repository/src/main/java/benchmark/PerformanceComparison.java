package benchmark;

import domain.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import repository.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class PerformanceComparison {
    ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
    ConcurrentHashMapBasedRepository<Integer, Order> concurrentHashMapBasedRepository = new ConcurrentHashMapBasedRepository<>();
    ECFastListBasedRepository<Order> ecFastListBasedRepository = new ECFastListBasedRepository<>();
    FastUtilObjectArrayBasedRepository<Order> fastUtilObjectArrayBasedRepository = new FastUtilObjectArrayBasedRepository<>();
    HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
    KolobokeHashSetBasedRepository<Order> kolobokeHashSetBasedRepository = new KolobokeHashSetBasedRepository<>();
    TreeSetBasedRepository<Order> treeSetBasedRepository = new TreeSetBasedRepository<>();

    private Order getOrder = new Order(123, 100, 20);
    
    //add operations
    @Benchmark
    public void addArrayList() {
        arrayListBasedRepository.add(getOrder);
    }

    @Benchmark
    public void addConcurrentHashMap() {
        concurrentHashMapBasedRepository.add(getOrder);
    }

    @Benchmark
    public void addEcFastList() {
        ecFastListBasedRepository.add(getOrder);
    }

    @Benchmark
    public void addHashSet() {
        hashSetBasedRepository.add(getOrder);
    }

    @Benchmark
    public void addKolobokeHashSet() {
        kolobokeHashSetBasedRepository.add(getOrder);
    }

    @Benchmark
    public void addTreeSet() {
        treeSetBasedRepository.add(getOrder);
    }

    @Benchmark
    public void addFastUtilObjectArray() {
        fastUtilObjectArrayBasedRepository.add(getOrder);
    }

    //contains operations
    @Benchmark
    public boolean containsArrayList() {
        return arrayListBasedRepository.contains(getOrder);
    }

    @Benchmark
    public boolean containsConcurrentHashMap() {
        return concurrentHashMapBasedRepository.contains(getOrder);
    }

    @Benchmark
    public boolean containsEcFastList() {
        return ecFastListBasedRepository.contains(getOrder);
    }

    @Benchmark
    public boolean containsHashSet() {
        return hashSetBasedRepository.contains(getOrder);
    }

    @Benchmark
    public boolean containsKolobokeHashSet() {
        return kolobokeHashSetBasedRepository.contains(getOrder);
    }

    @Benchmark
    public boolean containsTreeSet() {
        return treeSetBasedRepository.contains(getOrder);
    }

    @Benchmark
    public boolean containsFastUtilObjectArray() {
        return fastUtilObjectArrayBasedRepository.contains(getOrder);
    }

    //remove operations
    @Benchmark
    public void removeArrayList() {
        arrayListBasedRepository.remove(getOrder);
    }

    @Benchmark
    public void removeConcurrentHashMap() {
        concurrentHashMapBasedRepository.remove(getOrder);
    }

    @Benchmark
    public void removeEcFastList() {
        ecFastListBasedRepository.remove(getOrder);
    }

    @Benchmark
    public void removeHashSet() {
        hashSetBasedRepository.remove(getOrder);
    }

    @Benchmark
    public void removeKolobokeHashSet() {
        kolobokeHashSetBasedRepository.remove(getOrder);
    }

    @Benchmark
    public void removeTreeSet() {
        treeSetBasedRepository.remove(getOrder);
    }

    @Benchmark
    public void removeFastUtilObjectArray() {
        fastUtilObjectArrayBasedRepository.remove(getOrder);
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(PerformanceComparison.class.getSimpleName()).forks(1).build();

        new Runner(options).run();
    }

}
