package bigdecimal;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BigDecimalCalculator {
    private final List<BigDecimal> values;

    public BigDecimalCalculator(List<BigDecimal> values) {
        this.values = values;
    }

    public BigDecimal sum() {
        return values.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average() {
        if (Objects.isNull(values) || values.size() == 0) {
            throw new RuntimeException("Cannot divide by 0!");
        }
        return sum().divide(new BigDecimal(values.size()));
    }

    public List<BigDecimal> topTen() {
        double limit = values.size() * 0.10;
        return values.stream()
                .sorted(Comparator.reverseOrder())
                .limit((long) Math.ceil(limit))
                .collect(Collectors.toList());
    }
}
