package bigdecimal;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        List<BigDecimal> values = IntStream.range(0, 5)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());
        BigDecimalCalculator calculator = new BigDecimalCalculator(values);
        System.out.println(calculator.sum());
        System.out.println(calculator.average());
    }
}
