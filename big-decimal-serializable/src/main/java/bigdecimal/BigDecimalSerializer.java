package bigdecimal;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BigDecimalSerializer {

    public void serialize(List<BigDecimal> values, String filename) {
        try (FileOutputStream fos = new FileOutputStream(filename);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            values.stream().forEach(value -> {
                try {
                    oos.writeObject(value);
                    oos.flush();
                } catch (IOException e) {
                    //TODO: create custom exception
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            //TODO: create custom exception
            e.printStackTrace();
        }

    }

    public List<BigDecimal> deserialize(String filename) {
        List<BigDecimal> data = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(filename);
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            while (fis.available() > 0) {
                BigDecimal object = (BigDecimal) ois.readObject();
                data.add(object);
            }
        } catch (IOException | ClassNotFoundException e) {
            //TODO: create custom exception
            e.printStackTrace();
        }

        return data;
    }
}
