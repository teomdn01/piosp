package bigdecimal;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class BigDecimalSerializerTest {
    private final BigDecimalSerializer bigDecimalSerializer = new BigDecimalSerializer();

    @Test
    public void shouldSerializeAndDeserializeBigDecimalsArray() {
        int upperBound = 10_000;

        List<BigDecimal> values = IntStream.range(0, upperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        String filename = "values.ser";

        try {
            bigDecimalSerializer.serialize(values, filename);
            List<BigDecimal> deserialization = bigDecimalSerializer.deserialize(filename);

            assertEquals(deserialization.size(), values.size());
            assertEquals(deserialization, values);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
    }

}
