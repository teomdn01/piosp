package bigdecimal;


import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class BigDecimalCalculatorTest {
    private BigDecimalCalculator calculator;

    @Before
    public void setup() {
        List<BigDecimal> values = IntStream.range(0, 5)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());
         this.calculator = new BigDecimalCalculator(values);
    }

    @Test
    public void sumTest() {
        assertEquals(BigDecimal.TEN, calculator.sum());
    }

    @Test
    public void sumNullTest() {
        BigDecimalCalculator calculator = new BigDecimalCalculator(new ArrayList<>());
        assertEquals(BigDecimal.ZERO, calculator.sum());
    }

    @Test
    public void averageTest() {
        assertEquals(BigDecimal.valueOf(2), calculator.average());
    }

    @Test
    public void averageEmptyTest() {
        BigDecimalCalculator calculator = new BigDecimalCalculator(new ArrayList<>());
        assertThrows(RuntimeException.class, () -> calculator.average());
    }

    @Test
    public void topTenTest() {
        assertEquals(BigDecimal.valueOf(4), calculator.topTen().get(0));
    }

    @Test
    public void topEmptyTenTest() {
        assertEquals(0, new BigDecimalCalculator(new ArrayList<>()).topTen().size());
    }
}
