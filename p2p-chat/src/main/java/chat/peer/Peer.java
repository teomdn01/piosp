package chat.peer;

import chat.server.ServerThread;

import javax.json.Json;
import javax.json.JsonException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.Objects;

public class Peer {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("--> Enter username and port for current peer:");
        String[] config = bufferedReader.readLine().split(" ");

        ServerThread serverThread = new ServerThread(config[1]);
        serverThread.start();

        new Peer().updateListenToPeers(bufferedReader, config[0], serverThread);
    }

    private void updateListenToPeers(BufferedReader bufferedReader, String username, ServerThread serverThread) throws IOException {
        System.out.println("--> Enter HOSTNAME:PORT space separated values to receive messages from");
        System.out.println("--> Press 'S' to skip");

        String input = bufferedReader.readLine();
        String[] inputTokens = input.split(" ");

        if (!"s".equals(input)) {
            for (String inputToken : inputTokens) {
                String[] address = inputToken.split(":");
                Socket socket = null;
                try {
                    socket = new Socket(address[0], Integer.parseInt(address[1]));
                    new PeerThread(socket).start();
                } catch (IOException | NumberFormatException e) {
                    e.printStackTrace();
                    if (Objects.nonNull(socket))
                        socket.close();
                    else
                        System.out.println("Invalid input. Skipping to next peer address");
                }
            }
        }
        sendMessage(bufferedReader, username, serverThread);

    }

    public void sendMessage(BufferedReader bufferedReader, String username, ServerThread serverThread) {
        try {
            System.out.println("--> You can now communicate. Press E to exit. C to change peers");
            boolean flag = true;
            while (flag) {
                String message = bufferedReader.readLine();
                if ("e".equals(message)) {
                    flag = false;
                } else if ("c".equals(message)) {
                    updateListenToPeers(bufferedReader, username, serverThread);
                } else {
                    StringWriter stringWriter = new StringWriter();
                    Json.createWriter(stringWriter).writeObject(
                            Json.createObjectBuilder()
                                    .add("username", username)
                                    .add("message", message)
                                    .build());
                    serverThread.sendMessage(stringWriter.toString());
                }
            }
            System.exit(0);
        } catch (IOException | JsonException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
