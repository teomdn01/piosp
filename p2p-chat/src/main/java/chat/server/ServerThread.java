package chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

public class ServerThread extends Thread {
    private ServerSocket serverSocket;
    private Set<ServerThreadThread> serverThreadThreads = new HashSet<>();

    public ServerThread(String port) throws IOException {
        serverSocket = new ServerSocket(Integer.parseInt(port));
    }

    public Set<ServerThreadThread> getServerThreadThreads() {
        return serverThreadThreads;
    }

    @Override
    public void run() {
        try {
            while (true) {
                ServerThreadThread serverThreadThread = new ServerThreadThread(this, serverSocket.accept());
                serverThreadThreads.add(serverThreadThread);
                serverThreadThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        serverThreadThreads.forEach(t -> t.getPrintWriter().println(message));
    }
}
