package com.example.expression_helpers.cleaner;

import static com.google.common.base.Strings.isNullOrEmpty;

public class ExpressionCleaner implements IExpressionCleaner {

    @Override
    public String clean(String expression) {
        if (expression == null) {
            return null;
        }

        var trimmed = removeWhitespaces(expression);
        var lowerCaseTrimmed = trimmed.toLowerCase();
        var signed = addLeadingSignIfNotPresent(lowerCaseTrimmed);

        return signed;
    }

    private String removeWhitespaces(String expression) {
        if (expression == null) {
            return null;
        }

        return expression.replaceAll(" ", "");
    }

    private String addLeadingSignIfNotPresent(String expression) {
        if (isNullOrEmpty(expression)) {
            return null;
        }

        var firstCharacter = expression.charAt(0);

        if (firstCharacter != '+' && firstCharacter != '-') {
            var signedExpression = "+" + expression;
            return signedExpression;
        } else {
            return expression;
        }
    }
}
