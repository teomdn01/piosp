package com.example.expression_helpers.cleaner;

public interface IExpressionCleaner {

    String clean(String expression);

}
