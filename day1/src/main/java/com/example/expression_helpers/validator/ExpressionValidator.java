package com.example.expression_helpers.validator;

import com.google.common.base.Strings;

public class ExpressionValidator implements IExpressionValidator {

    @Override
    public boolean isExpressionValid(String expression) {
        return exists(expression) && hasValidFormat(expression);
    }

    private boolean exists(String expression) {
        return !Strings.isNullOrEmpty(expression);
    }

    private boolean hasValidFormat(String expression) {
        return expression.matches("^([0-9]*|(sqrt|min|max)*|([+\\-*/,()\\s])*)+$");
    }

}
