package com.example.expression_helpers.parser;


import com.example.enums.FactorType;
import com.example.enums.Operator;
import com.example.enums.Order;
import com.example.exceptions.InvalidCalculatorUsageException;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.enums.Operator.*;
import static com.google.common.base.Strings.isNullOrEmpty;


public class ExpressionParser implements IExpressionParser {

    private static final Map<String, Operator> SYMBOL_TO_OPERATOR_MAP = Map.ofEntries(
            new AbstractMap.SimpleEntry<>(PLUS.getSymbol(), PLUS),
            new AbstractMap.SimpleEntry<>(MINUS.getSymbol(), MINUS),
            new AbstractMap.SimpleEntry<>(MULTIPLY.getSymbol(), MULTIPLY),
            new AbstractMap.SimpleEntry<>(DIVIDE.getSymbol(), DIVIDE)
    );

    private static final String PLUS_OR_MINUS = "[\\" + PLUS.getSymbol() + MINUS.getSymbol() + "]";
    private static final String NOT_PLUS_OR_MINUS = "[^\\" + PLUS.getSymbol() + MINUS.getSymbol() + "]";

    private static final String MULTIPLY_OR_DIVIDE = "[\\" + MULTIPLY.getSymbol() + DIVIDE.getSymbol() + "]";
    private static final String NOT_MULTIPLY_OR_DIVIDE = "[^\\" + MULTIPLY.getSymbol() + DIVIDE.getSymbol() + "]";


    @Override
    public List<Pair<Operator, String>> divideBySecondOrderOperators(final String expression) {
        if (isNullOrEmpty(expression)) {
            return Lists.newArrayList();
        }

        var result = new ArrayList<Pair<Operator, String>>();

        var firstOrderTokens = extractFirstOrderTokens(expression);
        var signs = extractSigns(expression, Order.SECOND);

        if (firstOrderTokens.size() != signs.size()) {
            throw new InvalidCalculatorUsageException("Invalid parsing: tokens size must be equal to signs size!");
        }

        var size = firstOrderTokens.size();

        IntStream.range(0, size).forEach(index -> {
            var firstOrderToken = firstOrderTokens.get(index);
            var sign = signs.get(index);

            result.add(Pair.of(SYMBOL_TO_OPERATOR_MAP.get(sign), firstOrderToken));
        });

        return result;
    }

    private List<String> extractFirstOrderTokens(final String expression) {
        if (isNullOrEmpty(expression)) {
            return Lists.newArrayList();
        }

        return Arrays.stream(expression.split(PLUS_OR_MINUS))
                .filter(token -> !token.isEmpty())
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private List<String> extractSigns(final String expression, Order order) {
        if (isNullOrEmpty(expression)) {
            return Lists.newArrayList();
        }

        var split = order.equals(Order.FIRST)
                ? expression.split(NOT_MULTIPLY_OR_DIVIDE)
                : expression.split(NOT_PLUS_OR_MINUS);

        return Arrays.stream(split)
                .map(String::trim)
                .filter(token -> !token.isEmpty())
                .collect(Collectors.toList());
    }

    @Override
    public boolean isFirstOrderExpressionComposed(final String expression) {
        if (expression == null) {
            throw new InvalidCalculatorUsageException("Received null expression!");
        }

        return expression.split(MULTIPLY_OR_DIVIDE).length > 1;
    }

    @Override
    public List<String> divideByFirstOrderOperators(final String expression) {
        var tokens = List.of(expression.split(MULTIPLY_OR_DIVIDE));
        return tokens;
    }

    @Override
    public List<Operator> extractFirstOrderOperatorsInOrder(final String expression) {
        if (isNullOrEmpty(expression)) {
            return Lists.newArrayList();
        }

        var operators = extractSigns(expression, Order.FIRST)
                .stream()
                .map(SYMBOL_TO_OPERATOR_MAP::get)
                .collect(Collectors.toList());

        return operators;
    }

    @Override
    public FactorType getFactorType(final String factor) {
        if (factor.contains("sqrt")) {
            return FactorType.SQRT;
        } else if (factor.contains("min")) {
            return FactorType.MIN;
        } else if (factor.contains("max")) {
            return FactorType.MAX;
        } else {
            return FactorType.SIMPLE;
        }
    }
}
