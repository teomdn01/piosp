package com.example.calculator;

import com.example.exceptions.InvalidCalculatorUsageException;
import com.example.expression_helpers.cleaner.ExpressionCleaner;
import com.example.expression_helpers.cleaner.IExpressionCleaner;
import com.example.expression_helpers.parser.ExpressionParser;
import com.example.expression_helpers.parser.IExpressionParser;
import com.example.expression_helpers.validator.ExpressionValidator;
import com.example.expression_helpers.validator.IExpressionValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorIntegrationTest {

    private final IExpressionCleaner expressionCleaner = new ExpressionCleaner();

    private final IExpressionValidator expressionValidator = new ExpressionValidator();

    private final IExpressionParser expressionParser = new ExpressionParser();

    private final ICalculator calculator = new Calculator(expressionCleaner, expressionValidator, expressionParser);

    @ParameterizedTest
    @MethodSource("provideValidParameters")
    public void shouldComputeCorrectResultForValidInput(String expression, Integer expectedResult) {
        assertEquals(expectedResult, calculator.compute(expression));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "2++3", "  a ", "sqrt", "2+3+4///5", "2&3", "8+3/0-25"})
    public void shouldThrowInvalidCalculatorUsageExceptionForInvalidInput(String invalidExpression) {
        assertThrows(
                InvalidCalculatorUsageException.class,
                () -> calculator.compute(invalidExpression)
        );
    }

    private static Stream<Arguments> provideValidParameters() {
        return Stream.of(
                Arguments.of("-2+3/3-4*sqrt(16)/4+5*6*7*8", 1675),
                Arguments.of("5*7 / 35", 1),
                Arguments.of("sqrt(64)*3/12", 2),
                Arguments.of("-3-4-6", -13),
                Arguments.of("max(5,10)-min(20,50)", -10)
        );
    }
}
