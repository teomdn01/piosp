package com.example.expression_helpers;

import com.example.enums.Operator;
import com.example.expression_helpers.parser.ExpressionParser;
import com.example.expression_helpers.parser.IExpressionParser;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExpressionParserTest {

    private final IExpressionParser expressionParser = new ExpressionParser();

    @Test
    public void shouldDivideExpressionBySecondOrderOperators() {
        String signedExpression = "+  8*3/sqrt(2) + 9 - 54/2*min(4,8)";

        final List<Pair<Operator, String>> EXPECTED_PAIRS = Lists.newArrayList(
                Pair.of(Operator.PLUS, "8*3/sqrt(2)"),
                Pair.of(Operator.PLUS, "9"),
                Pair.of(Operator.MINUS, "54/2*min(4,8)")
        );
        final Integer EXPECTED_SIZE = 3;

        List<Pair<Operator, String>> pairs = expressionParser.divideBySecondOrderOperators(signedExpression);
        assertEquals(EXPECTED_SIZE, pairs.size());
        pairs.forEach(pair -> assertTrue(EXPECTED_PAIRS.contains(pair)));
    }


}
